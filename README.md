# 概要
Amplify開発用雛形

# 起動

```bash
docker compose build
docker compose up -d
```

# amplifyで使用するprofile設定
## profileを新たに作成する場合
コンテナに接続して `amplify configure` で対話的に入力

## 既存のプロファイルを使用する場合
以下のファイルを用意する

aws_configure/config
```
[profile your-profile-name]
region=ap-northeast-1
```

aws_configure/config

```
[your-profile-name]
aws_access_key_id=your-access-key
aws_secret_access_key=your-secret-access-key
```